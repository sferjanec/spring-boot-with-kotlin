package com.crispyecho.kotlinrest.repository

import com.crispyecho.kotlinrest.model.Article
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ArticleRepository : JpaRepository<Article, Long>